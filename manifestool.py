#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# OE manifest tool
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: GPL-2.0-only


import io, sys, os, time, re, subprocess, logging
import fcntl

logger = logging.getLogger(__name__)


def find_ipk(ipk_dir, p, r, v):
	"""
	Find an ipk file

	:param p: package name
	:param r: repo (architecture)
	:param v: version
	:return: path of ipk matching spec
	"""
	path = os.path.join(ipk_dir, "{}/{}_{}_{}.ipk".format(r, p, v, r))
	if os.path.exists(path):
		return path
	if ":" in v:
		path = os.path.join(ipk_dir, "{}/{}_{}_{}.ipk".format(r, p, v.split(":")[1], r))
		if os.path.exists(path):
			return path
	raise ValueError("Not found: pkg={} repo={} ver={} ({})".format(p, r, v, path))



def parse_opkg_info_as_manifest(path):
	"""
	Make manifest from /var/lib/opkg/info data
	"""
	out = set()

	if os.path.isdir(path):
		# Consider it's an opkg info dir
		for cwd, dirs, files in os.walk(path):
			files.sort()
			for filename in files:
				if not filename.endswith(".control"):
					continue

				path = os.path.join(cwd, filename)
				with io.open(path, "rb") as fi:
					data = fi.read()

				sig = data #hashlib.md5(data).digest()

				lines = data.decode("utf-8").splitlines()

				lines_out = list()
				for line in lines:
					if line.startswith(" "):
						lines_out[-1] += " " + line
					else:
						lines_out.append(line)

				d = dict()
				for line in lines_out:
					m = re.match(r"^(?P<k>\S+):( (?P<v>.*))?$", line)
					assert m is not None, "Line [{}]".format(line)
					k = m.group("k")
					v = m.group("v")
					d[k] = v

				out.add((d["Package"], d["Architecture"], d["Version"], sig))

	return out


def parse_manifest(path):
	"""
	Parse a manifest file (list of packages)
	"""
	with io.open(path, "rb") as f:
		data = f.read()

	results = set()
	for line in data.decode().splitlines():
		pkg, repo, version = line.rstrip().split()
		results.add((pkg, repo, version))

	return results


def parse_opkg_control(path):
	"""
	:param path: path of ipk
	"""

	import libarchive
	import libarchive.entry
	import libarchive.ffi


	with libarchive.file_reader(path, "ar") as archive:
		for entry in archive:
			if entry.name != "control.tar.gz":
				continue

			control_tarball_data = b"".join([block for block in entry.get_blocks()])
			break

	with libarchive.memory_reader(control_tarball_data, "tar", "gzip") as archive:
		for entry in archive:
			if entry.name != "./control":
				continue

			data = b"".join([block for block in entry.get_blocks()])
			break

	ret = dict()

	for line in data.decode("utf-8").splitlines():

		if "perl" in line:
			logger.debug("%s: %s", path, line)

		if line.startswith(" "):
			ret[k] += line
			continue

		k, v = line.split(": ", 1)
		ret[k] = v

	return ret


def depgraph(ipk_dir, manifest_path):
	"""
	"""
	import numpy as np
	import networkx as nx

	manifest_info = parse_manifest(manifest_path)

	g = nx.DiGraph()

	for pkg, _, _ in manifest_info:
		g.add_node(pkg)


	deps = list() # of (pkg, dep)

	done = set()

	sum_size = 0

	for pkg, repo, version in manifest_info:
		ipk_path = find_ipk(ipk_dir, pkg, repo, version)

		sz = os.path.getsize(ipk_path)
		g.nodes[pkg]["size"] = sz
		sum_size += sz

		ctl = parse_opkg_control(ipk_path)

		if not "Depends" in ctl:
			continue

		depends = ctl["Depends"].split(", ")

		for dep in depends:
			dep = dep.strip()

			logger.debug("Checking dependency “%s”", dep)
			m = re.match(r"^(?P<pkg>\S+)( \((?P<op>\S+) (?P<ver>\S+)\))?", dep)
			assert m is not None, dep
			dep = m.group("pkg")
			for pkg_, repo_, version_ in manifest_info:
				if pkg_ == dep:
					logger.debug("%s -> %s", pkg, dep)
					g.add_edge(pkg, dep)


	reorganize = dict(sorted(g.nodes.items(), key=lambda item: item[1]["size"], reverse=True))
	for key, value in reorganize.items():
		print("{}: {}".format(str(value["size"]).rjust(len(str(sum_size))), key))
	print("{}:".format(str(sum_size).rjust(len(str(sum_size)))))
	import matplotlib
	import matplotlib.figure
	import matplotlib.backend_bases

	def fig(dg):
		"""
		Produce a figure showing a station layout
		"""
		subpcfg = matplotlib.figure.SubplotParams(
		 left  =0.05,
		 bottom=0.05,
		 right =0.95,
		 top   =0.95,
		 wspace=0.00,
		 hspace=0.00,
		)
		figure = matplotlib.figure.Figure(
		 facecolor='white',
		 edgecolor='white',
		 subplotpars=subpcfg,
		 figsize=(32, 18),
		)

		axes = figure.add_subplot(1, 1, 1, frame_on=False)
		axes.get_xaxis().set_visible(False)
		axes.get_yaxis().set_visible(False)

		dg = dg.copy()

		# Hide some nodes?
		if 0:
			torem = []
			for x in dg.nodes():
				if dg.nodes[x].get("type", None) in ("...",):
					torem.append(x)

			dg.remove_nodes_from(torem)

		if 1:
			# TODO this is a hack
			old = nx.nx_pydot.to_pydot
			def new(x):
				a = old(x)
				#a.set("ordering", "in")
				#a.set("rotation", 
				return a
			nx.nx_pydot.to_pydot = new

			if 1:
				P = nx.nx_pydot.to_pydot(dg)
				#print(P.to_string())
				P.write_dot("deps.dot")

			# Layout hiearchically as GraphViz does (not random)
			pos = nx.nx_pydot.graphviz_layout(dg, prog='dot')

			for k, v in pos.items():
				x, y = v
				pos[k] = (-y,-x)
		else:
			pos = nx.spring_layout(dg)

		nodes = dg.nodes()
		node_colors = []
		for x in nodes:
			color = dg.nodes[x].get("color", (0.5, 0.5, 0.5, 0.25))
			node_colors.append(color)
		node_sizes = []
		for x in nodes:
			size = dg.nodes[x].get("size", 10)
			size = size * 1e-3#100 * np.log10(size)
			node_sizes.append(size)

		edges = dg.edges()
		edge_colors = []
		for x in edges:
			color = dg.edges[x].get("color", (0.5, 0.5, 0.5, 0.25))
			edge_colors.append(color)

			
		nx.draw_networkx(dg,
		 pos,
		 ax=axes,
		 node_color=node_colors,
		 edge_color=edge_colors,
		 node_size=node_sizes,
		 #style="dashed",
		 #arrows=True,
		 arrowstyle='-|>',
		 arrowsize=12,
		)

		figure.suptitle("Dependencies")
		return figure

	fig = fig(g)

	base = "deps"
	exts = (
	 "png",
	 "svg",
	)
	for ext in exts:
		canvas_class = matplotlib.backend_bases.get_registered_canvas_class(ext)
		canvas = canvas_class(fig)
		canvas_print = getattr(canvas, 'print_%s' % ext)
		canvas_print("%s.%s" % (base, ext))


def main():

	import argparse

	parser = argparse.ArgumentParser(
	 description="OE Tool",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("--build-dir",
	)

	parser.add_argument("--deploy-dir",
	)

	parser.add_argument("--ipk-dir",
	)

	parser.add_argument("--manifest",
	)

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	if args.build_dir is None:
		args.build_dir = "."

	if args.deploy_dir is None:
		args.deploy_dir = os.path.join(args.build_dir, "tmp", "deploy")

	if args.ipk_dir is None:
		args.ipk_dir = os.path.join(args.deploy_dir, "ipk")

	depgraph(args.ipk_dir, args.manifest)



if __name__ == '__main__':
	ret = main()
	raise SystemExit(ret)
