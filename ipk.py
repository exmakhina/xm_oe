#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# IPK metadata reader
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: GPL-2.0-only


import io, sys, os, time, re, subprocess, logging, shutil
import fcntl

logger = logging.getLogger(__name__)


def ipk_extract(path):
	"""
	:param path: path of ipk
	"""

	import libarchive
	import libarchive.entry
	import libarchive.ffi

	dest = "ipk"

	if os.path.exists(dest):
		shutil.rmtree(dest)
	os.makedirs(dest)

	with libarchive.file_reader(path, "ar") as archive:
		for entry in archive:
			print(entry)

			data = b"".join([block for block in entry.get_blocks()])

			with io.open(os.path.join(dest, entry.name), "wb") as fo:
				fo.write(data)

			if entry.name == "control.tar.gz":
				dirname = os.path.join(dest, "control")
				os.makedirs(dirname)
				with libarchive.memory_reader(data, "tar", "gzip") as archive:
					for entry in archive:
						data = b"".join([block for block in entry.get_blocks()])
						path = os.path.join(dirname, entry.name)
						with io.open(path, "wb") as fo:
							fo.write(data)

			elif entry.name == "data.tar.xz":
				dirname = os.path.join(dest, "data")
				os.makedirs(dirname)
				with libarchive.memory_reader(data, "tar", "xz") as archive:
					for entry in archive:
						path = os.path.join(dirname, entry.name)
						if entry.isdir:
							os.makedirs(path)
						else:
							data = b"".join([block for block in entry.get_blocks()])
							with io.open(path, "wb") as fo:
								fo.write(data)



def main():

	import argparse

	parser = argparse.ArgumentParser(
	 description="OE Tool",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("ipk",
	)

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	ipk_extract(args.ipk)



if __name__ == '__main__':
	ret = main()
	raise SystemExit(ret)
