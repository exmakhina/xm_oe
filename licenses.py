#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# License collector
# SPDX-FileCopyrightText: 2021 Jérôme Carretero <cJ@zougloub.eu>
# SPDX-License-Identifier: GPL-2.0-only


import io, sys, os, time, re, subprocess, logging
import collections

logger = logging.getLogger(__name__)


from manifestool import parse_manifest, find_ipk, parse_opkg_control


def licenses_summary(licenses_dir, ipk_dir, manifest_path):
	"""
	"""

	manifest_info = parse_manifest(manifest_path)

	pkg2lic = collections.defaultdict(lambda: set())

	for pkg, repo, version in manifest_info:

		ipk_path = find_ipk(ipk_dir, pkg, repo, version)

		ctl = parse_opkg_control(ipk_path)
		license = ctl["License"]

		recipe = ctl["OE"]
	
		recipeinfo_path = os.path.join(licenses_dir, recipe, "recipeinfo")
		if not os.path.exists(recipeinfo_path):
			print("pouet")

		if 1:
			d = dict()
			with io.open(recipeinfo_path, "r") as fi:
				for line in fi:
					k, v = line.strip().split(": ", 1)
					d[k] = v

			if d["LICENSE"] != license:
				logger.warning("Different license for pkg %s vs recipe %s: %s vs %s (could be normal)",
				 pkg, recipe, d["LICENSE"], license)

		logger.info("%s: %s", pkg, license)

		pkg2lic[pkg] = license

	all_licenses = set(pkg2lic.values())
	print(all_licenses)


	lic2count = collections.defaultdict(lambda: 0)
	for pkg, lic in pkg2lic.items():
		lic2count[lic] += 1

	for lic, count in sorted(lic2count.items(), key=lambda x: x[1]):
		logger.info("- %d: %s", count, lic)

	categories = {
		'ISC': ("attribution",),
		'LGPLv2.1 & GPLv2+': ("GPL",),
		'BSD | GPLv2': ("BSD",),
		'openssl': ("",),
		'LGPLv3+ | GPLv2': ("GPL",),
		'GPLv2 & bzip2-1.0.4': ("GPL",),
		'GPLv3': ("GPL",),
		'CLOSED': (),
		'PD': ("",),
		'bzip2-1.0.6': ("",),
		'GPLv2+ & LGPLv2.1+': ("GPL",),
		'GPLv3+': ("GPL",),
		'GPL-2.0+ & LGPL-2.1+': ("GPL",),
		'LGPLv2.1+ & BSD & PD': ("GPL",),
		'BSD | Artistic-1.0': ("attribution",),
		'(GPLv2+ | LGPLv3)': ("GPL",),
		'GPLv2+ | BSD': ("BSD",),
		'AFL-2.1 | GPLv2+': ("GPL",),
		'LGPL-2.1+': ("GPL",),
		'PSFv2': (),
		'LGPLv3+': ("GPL",),
		'MIT': ("attribution",),
		'GPLv2': ("GPL",),
		'GPLv2 & LGPLv2 & BSD & MIT': ("GPL",),
		'GPL-3.0-with-GCC-exception': ("GPL",),
		'BSD-3-Clause': ("attribution",),
		'LGPLv3+ | GPLv2+': ("GPL",),
		'Zlib': ("attribution",),
		'LGPLv2.1+': ("GPL",),
		'BSD-3-Clause & BSD-2-Clause & PSF & Apache-2.0 & BSD & MIT': ("attribution",),
		'BSD-0-Clause': (),
		'Apache-2.0': ("attribution",),
		'GPLv2 & GPLv3 & LGPLv2 & LGPLv3': ("GPL",),
		'MPL-2.0 | (MPL-2.0 & GPL-2.0+) | (MPL-2.0 & LGPL-2.1+)': ("GPL",),
		'LGPLv2.1': ("GPL",),
		'GPL-2.0 | MPL-2.0 | LGPL-2.1': ("GPL",),
		'GPLv2+': ("GPL",),
		'GPLv2 | LGPLv3+': ("GPL",),
		'GPLv2+ & LGPLv2.1+ & BSD-3-Clause & BSD-4-Clause': ("GPL",),
		'GPLv2+ | LGPLv3+': ("GPL",),
		'LGPL-2.1': ("GPL",),
		'GPLv2 & LGPLv2.1': ("GPL",),
		'BSD & ISC & MIT': ("attribution",),
	}

	pkg2cat = dict()
	for pkg, lic in pkg2lic.items():
		pkg2cat[pkg] = categories[lic]

	cat2count = collections.defaultdict(lambda: 0)
	for pkg, cats in pkg2cat.items():
		for cat in cats:
			cat2count[cat] += 1

	for cat, count in sorted(cat2count.items(), key=lambda x: x[1]):
		logger.info("- %d: %s", count, cat)

	
def main():

	import argparse

	parser = argparse.ArgumentParser(
	 description="OE Tool",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("--build-dir",
	)

	parser.add_argument("--deploy-dir",
	)

	parser.add_argument("--ipk-dir",
	)

	parser.add_argument("--licenses-dir",
	)

	parser.add_argument("--manifest",
	)

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	if args.build_dir is None:
		args.build_dir = "."

	if args.deploy_dir is None:
		args.deploy_dir = os.path.join(args.build_dir, "tmp", "deploy")

	if args.ipk_dir is None:
		args.ipk_dir = os.path.join(args.deploy_dir, "ipk")

	if args.licenses_dir is None:
		args.licenses_dir = os.path.join(args.deploy_dir, "licenses")

	licenses_summary(args.licenses_dir, args.ipk_dir, args.manifest)



if __name__ == '__main__':
	ret = main()
	raise SystemExit(ret)
